# Instructions

```
git clone git@bitbucket.org:dylanclaykendal/emoji-picker.git
npm install
npm start
```

Then visit http://localhost:1234


# Summary

The biggest challange I had on this project was underestimating how much effort
the typical react-typescript-postcss-bundling boilerplate would be, which ended
up taking up the entire first hour an a half. This feels like a pretty typical case,
where you hope that gambling on a choice will be faster in the long run
(spoilers, it's not lol). In this case, although I'm really familiar with
Webpack, I also know that it can take a lot of fiddling to get everything to
work properly, I had used parcel in the past so I decided to _believe_ that
it's No Config Necessary™ would be true. It worked out of the box for jsx, tsx,
and providing a web server so I thought I was in the clear and then I tried to
get it to start compiling postcss which led to an package upgrade etc etc - didn't
end up working and didn't have time to find out why. After burning time trying to
get postcss to work with parcel, which was with the promise of saving time by
using tailwindcss, I opted to just included a fully packaged, 3MB version of tailwind
so I could actually get started.

As far as actual project work goes it was fairly smooth sailings. I opted to just
use whatever baked in state management comes with React so I didn't have to burn
any additional time. Was using React hooks for the first time but they were pretty
inuitive so no issues there. For implementation choices the only big things I can
list would be the choice of layout for the emojis, and how the click-out is 
handled for the picker: for the layout I opted to use css grid, easiest for
me to implement, no modulo'ing indices to manually create rows and columns. Obviously
doesn't have support everywhere but not really relevant for this exercise.
for the click-out I chose a fairly standard approach of set a click handler for
a top level containing element, on each click walk up each node's parent to see
if you're current inside the emoji-modal. An similar alternative would be to 
check the bounding rectangle of the modal and see if the click event falls within
it. Lastly a more exhaustive approach would be to force focus, and do a similar
procedure only checking that the focus remains within the modals children.

# Time logs

Unless otherwise mentioned each block is 15 minutes,

- 20:00 blah blah blah

Would be for the interval 19:45 - 20:00.

- 20:00 Looking at skelton app, reading docs on unicode spec and emoji
- 20:15 Reading through some docs on unicode v13.0, foudn some data files that list the entire collection. just initialized a new repo
- 20:30 Don't feel like fidding w/ rollup or webpack to get things to work, will see if parcel works out of the box
- 20:45 Mostly just boilerplate, thankfully parcel worked out of the box with typescript this time, setting all the normal react stuff up

- 21:00 - 21:45 Phone call / break

- 22:00 Got the tailwijnd plugin to work had to upgrade parcel to v2 because of postcss version issue
- 22:15 Started rendering out basic emoji data list taken from template app
- 22:30 Applying some basic structural styling. Went to use tailwind trying to figure out why parcel won't bundle any postcss

- 22:45 Break

- 23:00 Gave up on trying to get postcss to work just gonna inlcude the full tailwind stylesheets
- 23:15 More stylesheet wackness regret not sticking to webpack. Seems to be working now used tailwind cli to output bundled version
- 23:30 Clean up some of the visual elements
- 23:45 Tweak column layout, add basic on click handle to report the selection
- 00:00 Set a fixed height on the modal and overflow options
- 00:15 Set Added basic popover with react-popover not gonna worry about making it pretty right now
- 00:30 Working on getting clicking outside to close the modal
- 01:00 Finished the click-out behaviour

Total time: 3:45

---

# Requirments

- bind to some element to open on click, position must be relative to element
- send some event with the selected emoji

# Tasks

- [X] Boiler plate
- [X] List all emoji in container
Just going to start with the list of emoji that are in https://bitbucket.org/charley-remotion/emoji-picker-project-skeleton/src/master/src/components/emojis.js
So I don't have to do any data munging
- [X] Send event on click
- [X] bind on click to a button to open picker
- [X] close picker on click

Optional
1. Emoji Search
2. Tooltips w/ names
3. Click out to exit
4. Emoji skintone implementation
5. Non-unicode emoji (ie. images, emoji packs, etc)
6. Anything you can think of!

# Notes

There's a number of emoji that have multiple code points, it would be interesting
to do some kind of radial picker for those if there's time like:

1F64B 1F3FB 200D 2640 FE0F                 ; fully-qualified     # 🙋🏻‍♀️ woman raising hand: light skin tone

Not all of these display properly for me though in firefox or chrome.

