import * as React from 'react'
import { useState, useRef } from 'react'
import Popover from 'react-popover'
import { emojis } from './emoji'

const entries: [[keyof typeof emojis, string[]]] = Object.entries(emojis) as any

interface EmojiPickerProps {
  onSelection: (emoji: string) => void
  pickerRef: React.MutableRefObject<null>
}

const EmojiPicker = ({ onSelection, pickerRef }: EmojiPickerProps) => {
  return (
    <div
      ref={pickerRef}
      className="p-2 m-2 overflow-y-auto border border-gray-700 border-solid hide-scroll max-h-72 w-60 rounded-md"
      tabIndex={0}
    >
      {entries.map(([category, values]) => (
        <>
          <div key={category} className="font-bold capitalize">
            {category}
          </div>
          <div key={`${category}-values`} className="grid grid-cols-8 gap-2">
            {values.map(emoji => (
              <div key={emoji} className="" onClick={() => onSelection(emoji)}>
                {emoji}
              </div>
            ))}
          </div>
        </>
      ))}
    </div>
  )
}

const App = () => {
  const [isOpen, setIsOpen] = useState(false)
  const pickerRef = useRef(null)

  const onClick = (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    if (!isOpen) {
      return
    }

    let target: null | HTMLElement = e.target as HTMLElement

    while (target != null) {
      if (target == pickerRef.current) {
        return
      }
      target = target.parentElement
    }

    setIsOpen(false)
  }

  return (
    <div className="w-full h-full p-10" onClick={onClick}>
      <Popover
        isOpen={isOpen}
        body={[
          <EmojiPicker
            onSelection={emoji => console.log(emoji)}
            pickerRef={pickerRef}
          />,
        ]}
      >
        <button
          className="px-3 py-1 bg-blue-200 border border-blue-300 rounded-md"
          onClick={() => setIsOpen(!isOpen)}
        >
          Click me
        </button>
      </Popover>
    </div>
  )
}

export default App
